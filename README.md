# Boilerplate: CRA V2 + Electron

## Instructions

- Download this repository
- You must have installed [npm](https://www.npmjs.com) and [node](https://nodejs.org/es/)
- In the root of project, install the dependencies: `npm install`
- Execute this command to start the application: `npm run dev`
- Develop your application
- Finally, to compile the application use the command: `npm run ebuild`

---

## Comments

- This project uses the latest version of Create React App i.e V2
- The builds have been tested as well and a packaged app does not need a web server to run. You can try it using `npm run ebuild`
- For dev environment the project uses [Foreman](https://www.npmjs.com/package/foreman) to run two processes react-scripts start and Electron
- Instead of Foreman you can also reconfigure it to use 'concurrently', if thats your favorite tool persnickety about it

## More info & credits

Christian Sepulveda

- Blog post: [How to build an Electron app using create-react-app. No webpack configuration or “ejecting” necessary](https://medium.freecodecamp.org/building-an-electron-application-with-create-react-app-97945861647c)
